class recursion{
	void fun (int num){
		if(num==0)
			return;
		System.out.println(num);
		fun(--num);
	}
	public static void main (String[]args){

		System.out.println("Start main ");
		recursion obj=new recursion();
		obj.fun(2);

		System.out.println("End main");
	}
}
