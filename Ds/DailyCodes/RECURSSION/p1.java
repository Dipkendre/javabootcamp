class Recursion{

	void NumPrint(int num){
	
		if(num>10)
			return;
		System.out.println(num);
		
		NumPrint(--num);
	
	
	}

	public static void main (String[]args){
	
	
		Recursion obj =new Recursion();
	
		obj.NumPrint(10);
	}
}
		
