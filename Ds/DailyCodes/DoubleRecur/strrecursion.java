class recursion{

	String revstr(String str){
		
		if(str==null||str.length()<=1){
			return str;
		}
		return revstring(str.subString(1))+str.charAt(0);
	}
	public static void main (String[]arg){
	
		String str="core2web";

		recursion obj=new recursion();
		String reverse=obj.revStr(str);

		System.out.println(reverse);
	}
	
}

