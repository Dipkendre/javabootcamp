class recursion{
	
	int fun(int num){
		if(num<=1)
			return ;
		fun(num-2);
		
		System.out.println(num);
		fun(num-1);

	}

	public static void main(String[]args){
		recursion obj =new recursion();
		int ret=obj.fun(5);

		System.out.println(ret);
	}

}
