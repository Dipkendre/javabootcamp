//WAP to display the first 10 natural numbers in reverse order.

class Reverse{
	
	static void numRev(int num){
	
		if(num==0)
			return;
	
		System.out.println(num);
		numRev(num-1);

	
	}
	public static void main (String[]args){
	
		numRev(10);
	

	}
}

