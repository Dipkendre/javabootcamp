class JaggArray{
        public static void main (String[]args){

		// Jagged Array initialiation 
		// 1 st way 
                int arr1[][]={{1,2,3},{4,5},{7}};

		// 2 nd way 
		int arr[][]=new int[3][];

		arr[0]=new int []{1,2,3};
		arr[1]=new int []{4,5};
		arr[2]=new int []{7};
	}
}

