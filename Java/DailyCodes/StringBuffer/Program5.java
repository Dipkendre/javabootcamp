class StringBufferDemo{

        public static void main (String[]args){

                String str1="Shashi";
                String str2=new String("Shashi");
                StringBuffer str3=new StringBuffer ("Core2web");
                
		String str4=str1.concat(str3); //Error: incompatible types: StringBuffer cannot be converted to String

                StringBuffer str5=new StringBuffer(str2);

                System.out.println(str1);
                System.out.println(str2);
                System.out.println(str3);
                System.out.println(str4);
                System.out.println(str5);
        }
}
