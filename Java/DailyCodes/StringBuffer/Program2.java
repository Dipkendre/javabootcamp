class StringBufferDemo{

        public static void main (String[]args){

                StringBuffer sb=new StringBuffer(100);

                sb.append("binecaps");
                sb.append("core2web");

                System.out.println(sb.capacity());
                System.out.println(sb);

                sb.append("Incubator");

                System.out.println(sb.capacity());
                System.out.println(sb);
        }
}
