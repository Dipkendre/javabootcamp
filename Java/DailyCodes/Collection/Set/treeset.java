import java.util.*;
class Movies implements Comparable {
	
	String MovieName=null;
	float totalColl=0.0f;

	Movies(String MovieName ,float totalColl){
		
		this.MovieName=MovieName;
		this.totalColl=totalColl;
	}
	public int compareTo (Object obj ){
	
		return MovieName.compareTo(((Movies)obj).MovieName);

	}
	public String toString(){
	
		return MovieName+totalColl;
	}
	
}
class TreesetDemo{

	public static void main(String[]Dk){
		
		TreeSet ts=new TreeSet();
		
		ts.add(new Movies ("Gadar",150.0f));
		ts.add(new Movies ("OMG",120.00f));
		ts.add(new Movies ("Jailer",200.00f));

		System.out.println(ts);
	}
}
			
