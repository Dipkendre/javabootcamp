import java.util.*;
class Employee{

	String EmpName=null;
	float sal=0.0f;

	Employee (String EmpName, float sal){
		this.EmpName=EmpName;
		this.sal=sal;
	}
	public String toString(){
		return "{" + EmpName +":" +sal+"}";

	}
}
class SortByName implements Comparator<Employee>{
	public int compare(Employee obj1, Employee obj2){

		return obj1.EmpName.compareTo(obj2.EmpName);
	}
}
class SortBysal implements Comparator<Employee>{
	public int compare(Employee obj1, Employee obj2){

		return (int) (obj1.sal-obj2.sal);
	}
}
class ListsoerArray{

	public static void main (String[]args){
		
		ArrayList<Employee> al=new ArrayList<Employee>();

		al.add(new Employee ("Jay",200000.00f));
		al.add(new Employee ("Ganu",300000.00f));
		al.add(new Employee ("Ronny",400000.00f));
		al.add(new Employee ("Abhi",500000.00f));

		System.out.println(al);

		Collection.sort(al,new SortByName());
		System.out.println(al);
		
	}
}


