import java.util.*;

class Treemap{

	public static void main (String[]args){

		SortedMap tm=new TreeMap();
	
		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("Slk","Srilanka");
		tm.put("Ban","bangladesh");
		tm.put("Aus","Austrila");

		System.out.println(tm);

		//headmap
		System.out.println(tm.headMap("Pak"));
				
		//tailmap
		System.out.println(tm.tailMap("Pak"));


		//firatkey
		System.out.println(tm.firstKey());
		
		//lastkey
		System.out.println(tm.lastKey());

		//keyset
		System.out.println(tm.keySet());

		//values
		System.out.println(tm.values());

	}
}
