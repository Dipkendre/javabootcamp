import java.util.*;
class Platform implements Comparable{

        String str=null;
        int foundYear=0;

        Platform(String str,int foundYear){

                this.str=str;
                this.foundYear=foundYear;
        }
        public String toString(){
                return str + foundYear;
        }
        public int compareTo(Object obj){
                return this.foundYear-((Platform)obj).foundYear;
        }
	class sortByName implements Comparator{

		public int compare(Object obj1, Object obj2){

			return ((Platform )obj1).str.compareTo(((Platform)obj2).str);
		}
	}

}

class TreemapDemo{
        public static void main (String[]args){

                TreeMap tm=new TreeMap(new sortByName());

                tm.put(new Platform ("Youtube",2005),"Google");
                tm.put(new Platform ("Insta",2010),"Meta");
                tm.put(new Platform ("Facebook",2004),"Meta");
                tm.put(new Platform ("Chatgpt",2022),"OpenAI");

                System.out.println(tm);
        }
}

