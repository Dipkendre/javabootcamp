class outer {
	void m1(){
	
		System.out.println("In m1-outer");

		//method local inner class
		class Inner{
			void m1(){
				System.out.println("In m1-inner");
			}
		}
		Inner obj=new Inner();
		obj.m1();

	}
	void m2(){

		System.out.println("In m2-outer");
	}
	public static void main (String[]args){
	
		outer obj=new outer();
		obj.m1();
		obj.m2();
	}
}
