class Mythread extends Thread{

        Mythread(ThreadGroup tg,String str){
                super(tg,str);
        }
        public void run(){

                System.out.println(Thread.currentThread());
		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
        }
}
class ThreadGroupDemo{

        public static void main (String[]args){

                ThreadGroup pThreadGp=new ThreadGroup("India");

                Mythread t1=new Mythread(pThreadGp,"Maha");
                Mythread t2=new Mythread(pThreadGp,"Goa");
                Mythread t3=new Mythread(pThreadGp,"UP");

                t1.start();
                t2.start();
                t3.start();

                
                ThreadGroup cThreadGp=new ThreadGroup( pThreadGp,"Pakistan");

                Mythread t4=new Mythread(pThreadGp,"Lahor");
                Mythread t5=new Mythread(pThreadGp,"Karachi");
                Mythread t6=new Mythread(pThreadGp,"kabul");

                t4.start();
                t5.start();
                t6.start();

                ThreadGroup cThreadGp2=new ThreadGroup( pThreadGp,"BanglaDesh");

                Mythread t7=new Mythread(pThreadGp,"Dhaka");
                Mythread t8=new Mythread(pThreadGp,"Manipur");
                Mythread t9=new Mythread(pThreadGp,"banga");

                t7.start();
                t8.start();
                t9.start();
		
		cThreadGp.interrupt();

		System.out.println(pThreadGp.activeCount());
		System.out.println(pThreadGp.activeGroupCount());
        }
}

