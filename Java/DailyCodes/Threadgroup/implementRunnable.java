class Mythread implements Runnable{
        public void run(){

                System.out.println(Thread.currentThread());
                try{
                        Thread.sleep(5000);
                }catch(InterruptedException ie){
                        System.out.println(ie.toString());
                }
        }
}
class ThreadGroupDemo{

        public static void main (String[]args){

                ThreadGroup pThreadGp=new ThreadGroup("India");

		Mythread obj1=new Mythread();
		Mythread obj2=new Mythread();

		Thread t1=new Thread(pThreadGp,obj1,"Maha");
		Thread t2=new Thread(pThreadGp,obj2,"Gao");

		t1.start();
		t2.start();
	}
}


