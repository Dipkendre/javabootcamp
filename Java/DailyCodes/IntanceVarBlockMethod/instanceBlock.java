class Demo{

	int x=10;
	Demo(){
		System.out.println("Constructor");
	}
	//instance block
	{
		System.out.println("in Instance block 1");
	}
	public static void main (String[]args){
	
		Demo obj=new Demo();
		System.out.println("In main method");
	}
	//instance block
	{
		System.out.println("in Instance block 2");
	}
}
