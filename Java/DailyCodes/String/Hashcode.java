class HashcodeDemo{
	public static void main (String[]args){

		String str1="Dipak";
		String str2=new String("Dipak");
		String str3="Dipak";
		String str4=new String("Dipak");

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
	}
}
