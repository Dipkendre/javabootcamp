#include <stdio.h>

void fun(int x) {
  printf("%d\n", x);
  
  fun(++x);
}

int main() {
  fun(1);
 
}
