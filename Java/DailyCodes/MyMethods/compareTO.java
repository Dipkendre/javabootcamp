import java.io.*;
class CompareToDemo{
	static int mystrLen(String str){
		char arr[]=str.toCharArray();
		int count=0;

		for(int i=0;i<arr.length;i++){
			count++;
		}
		return count;
	}
	static int mystrComp(String str1,String str2){

		char arr1[]=str1.toCharArray();
		char arr2[]=str2.toCharArray();
		
		int difference=0;
		
		int length1=mystrLen(str1);
		int length2=mystrLen(str2);
		
		if(length1 == length2){

			for(int i=0;i<arr1.length;i++)
				if(arr1[i]!=arr2[i]){
					
					difference=arr1[i]-arr2[i];
					break;
				}
		}	
		else{
		
			difference=length1-length2;
		}
		return difference;
	}
	public static void main (String[]args)throws IOException{
	
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));	

		System.out.println("Enter first String");
		String str1=br.readLine();

		System.out.println("Enter second String");
		String str2=br.readLine();

		System.out.println(mystrComp(str1,str2));
	}
}


