/* Write a program, take 7 characters as an input , Print only vowels from the array
Input: a b c o d p e
Output :a  o  e*/



import java.util.*;

class Vowels{
	public static void main(String sach[]){

		int sum=0;

		Scanner br = new Scanner(System.in);
		System.out.println("Enter size of array ");

		int size = br.nextInt();
		char carr[] = new char[size];

		System.out.println("Enter array Elements");
		for(int i=0;i<size;i++){
			carr[i]= br.next().charAt(0);
		}
		for(int i=0;i<carr.length;i++){

			if(carr[i]=='a'||carr[i]=='e'||carr[i]=='i'||carr[i]=='o'||carr[i]=='u'||carr[i]=='A'||carr[i]=='E'||carr[i
			]=='I'||carr[i]=='O'||carr[i]=='U'){

				System.out.print(carr[i]+" ");
			}
		}

		System.out.println();

	}
}
