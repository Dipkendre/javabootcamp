/*WAP to find the number of even and odd integers in a given array of integers
Input: 1 2 5 4 6 7 8
Output:
Number of Even Elements: 4
Number of Odd Elements : 3
*/
import java.io.*;
class EvenOdd{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new
				InputStreamReader(System.in));
		System.out.println("Enter size of array");

		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];

		System.out.println("Enter elements in the array");
		for(int i=0; i<arr.length; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}
		int evencount = 0;
		int oddcount = 0;
		for(int i=0; i<arr.length; i++) {
		
			if(arr[i]%2 == 0) {
			
				evencount++;
		
			} else {
			
				oddcount++;
			}
		}
		System.out.println("Number of even elements = "+evencount);
		System.out.println("Number of odd elements = "+oddcount);
	}
}
